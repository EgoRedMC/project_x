﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Project_X
{
    class Matrix
    {
        Random r = new Random();
        private int Height = 1;
        private int Width = 1;
        public int[][] Data;
        //
        public Matrix()
        {
            Data = new int[Height][];
            for (int i = 0; i < Height; i++)
            {
                Data[i] = new int[Width];
                for (int j = 0; j < Width; j++)
                    Data[i][j] = 0;
            }
        }
        public Matrix(int HeightWidth)
        {
            Height = HeightWidth;
            Width = HeightWidth;
            Data = new int[Height][];
            for (int i = 0; i < Height; i++)
            {
                Data[i] = new int[Width];
                for (int j = 0; j < Width; j++)
                    Data[i][j] = 0;
            }
        }
        public Matrix(int height, int width)
        {
            Height = height;
            Width = width;
            Data = new int[Height][];
            for (int i = 0; i < Height; i++)
            {
                Data[i] = new int[Width];
                for (int j = 0; j < Width; j++)
                    Data[i][j] = 0;
            }
        }
        public Matrix(string filename)
        {
            StreamReader stream1 = new StreamReader(filename);
            string line = null;
            Height = 0;
            while ((line = stream1.ReadLine()) != null)
            {
                Height++;
                Width = line.Length;
            }
            line = null;
            stream1.Close();
            Data = new int[Height][];
            for (int i = 0; i < Height; i++)
            {
                Data[i] = new int[Width];
                for (int j = 0; j < Width; j++)
                    Data[i][j] = 0;
            }
            StreamReader stream2 = new StreamReader(filename);
            int num = 0;
            while ((line = stream2.ReadLine()) != null)
            {
                for (int i = 0; i < line.Length; i++)
                    Data[num][i] = int.Parse(line[i].ToString());
                num++;
            }
            stream2.Close();
        }
        //
        public void ReadFromFile(string filename)
        {
            StreamReader stream1 = new StreamReader(filename);
            string line = null;
            Height = 0;
            while ((line = stream1.ReadLine()) != null)
            {
                Height++;
                Width = line.Length;
            }
            line = null;
            stream1.Close();
            Data = new int[Height][];
            for (int i = 0; i < Height; i++)
            {
                Data[i] = new int[Width];
                for (int j = 0; j < Width; j++)
                    Data[i][j] = 0;
            }
            StreamReader stream2 = new StreamReader(filename);
            int num = 0;
            while ((line = stream2.ReadLine()) != null)
            {
                for (int i = 0; i < line.Length; i++)
                    Data[num][i] = int.Parse(line[i].ToString());
                num++;
            }
            stream2.Close();
        }
        public int GetHeight()
        {
            return Height;
        }
        public int GetWidth()
        {
            return Width;
        }
        public void RandomizeMatrix()
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                    Data[i][j] = r.Next();
            }
        }
        public void RandomizeMatrix(int max)
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                    Data[i][j] = r.Next(max);
            }
        }
        public void RandomizeMatrix(int min, int max)
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                    Data[i][j] = r.Next(min, max);
            }
        }
        public void Print()
        {
            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                    Console.Write(Data[i][j]);
                Console.WriteLine();
            }
        }
    }
}
