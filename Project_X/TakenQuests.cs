﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class TakenQuests
    {
        public QuestGo[] Taken = new QuestGo[10000];
        public int count = 0;
        //
        public TakenQuests()
        { }
        //
        public void EraseQuest(int num)
        {
            for (int i = num; i < count; i++)
                Taken[i] = Taken[i + 1];
            Taken[count--] = null;
        }
        public void PutInQuest(QuestGo quest)
        {
            Taken[count++] = quest;
        }
        public void ShowTakenQuests(int usinglevel, NPCOnLevel npc, Player player, Levels levels)
        {
            Console.Clear();
            bool c = true;
            while (c)
            {
                int noQuests = 0;
                for (int i = 0; i < count; i++)
                    if (Taken[i] == null)
                        noQuests++;
                if (noQuests == count)
                    c = false;
                if (noQuests != count)
                {
                    Console.WriteLine("Задачи");
                    for (int i = 0; i < count; i++)
                    {
                        if (Taken[i] != null)
                        {
                            Console.WriteLine(" [ ] {0}", Taken[i].QuestName);
                        }
                    }
                    Console.WriteLine(" [ ] Выход");
                    ConsoleKeyInfo key;
                    bool b = true;
                    int num = 0;
                    Console.SetCursorPosition(2, num+1);
                    Console.Write(" ");
                    while (b)
                    {
                        Console.SetCursorPosition(2, num+1);
                        Console.Write("x");
                        key = Console.ReadKey(true);
                        Console.SetCursorPosition(2, num+1);
                        Console.Write(" ");
                        switch (key.Key)
                        {
                            case ConsoleKey.UpArrow:
                                if (num > 0)
                                    num--;
                                break;
                            case ConsoleKey.DownArrow:
                                if (num < count)
                                    num++;
                                break;
                            case ConsoleKey.J:
                                b = false;
                                c = false;
                                break;
                            case ConsoleKey.Escape:
                                b = false;
                                c = false;
                                break;
                            case ConsoleKey.Enter:
                                if (num == count)
                                {
                                    b = false;
                                    c = false;
                                    break;
                                }
                                Console.Clear();
                                Console.WriteLine(Taken[num].QuestTalk);
                                Console.WriteLine("Найти его вы можете на координатах {0} {1}", Taken[num].npcEnd.Y + 1, Taken[num].npcEnd.X + 1);
                                Console.WriteLine("1. Обратно");
                                bool wtf = true;
                                while (wtf)
                                {
                                    ConsoleKeyInfo key2 = Console.ReadKey(true);
                                    if ((key2.Key == ConsoleKey.Escape) | (key2.Key == ConsoleKey.D1))
                                        break;
                                }
                                Console.Clear();
                                Console.WriteLine("Задачи");
                                for (int i = 0; i < count; i++)
                                {
                                    if (Taken[i] != null)
                                    {
                                        Console.WriteLine(" [ ] {0}", Taken[i].QuestName);
                                    }
                                }
                                Console.WriteLine(" [ ] Выход");
                                break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Активных квестов нет");
                    Console.WriteLine(" [x] Выход");
                    ConsoleKeyInfo key1;
                    while (true)
                    {
                        key1 = Console.ReadKey(true);
                        if ((key1.Key == ConsoleKey.J) | (key1.Key == ConsoleKey.Escape)|(key1.Key==ConsoleKey.Enter))
                            break;
                    }
                }
                Console.Clear();
                levels.Print(usinglevel);
                npc.Print(levels, player);
            }
        }
    }
}
