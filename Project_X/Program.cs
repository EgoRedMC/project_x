﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
     class Program
    {
        static Random r = new Random();
        static public NPCOnLevel[] npc = new NPCOnLevel[10];
        static public void ClearLine(int line)
        {
            Console.SetCursorPosition(0, line + 1);
            for (int i = 0; i < 79; i++)
                Console.Write(" ");
            Console.SetCursorPosition(0, line + 1);
        }
        static void Main(string[] args)
        {
            int usinglevel = 0;
            Levels levels = new Levels(10);
            Player player = new Player();
            Controls controls = new Controls();
            npc[0] = new NPCOnLevel("NPCOnLevel0.txt");
            npc[0].SetQuests("NPCOnLevel0.txt");
            while (true)
            {
                levels.Print(usinglevel);
                npc[usinglevel].Print(levels, player);
                //
                while (true)
                {
                    player.Point.PlaceThis(levels);
                    controls.key = Console.ReadKey(true);
                    controls.WhatButton();
                    controls.ButtonDo(usinglevel, npc[usinglevel], player, levels);
                }
            }
        }
    }
}
