﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class QuestGo
    {
        public NPC npcStart = new NPC();
        public NPC npcEnd = new NPC();
        public bool Done = false;
        public bool Took = false;
        public string QuestName = "Прогулка";
        public string QuestTalk = "Сходи к нему и всё";
        public string QuestTalkEnd = "Спасибо тебе за помощь";
        public int Exp = 20;
        //
        public QuestGo(NPC npcstart, NPC npcend,string questname,string questtalk,string questtalkend, int exp)
        {
            npcEnd = npcend;
            npcStart = npcstart;
            QuestName = questname;
            QuestTalk = questtalk;
            QuestTalkEnd = questtalkend;
            Exp = exp;
        }
        public QuestGo()
        { }
        //
        public void Did()
        {
            if (Took)
            {
                Done = true;
                Console.WriteLine("Квест выполнен");
                Console.WriteLine("Получено очков опыта: {0}",Exp);
            }
        }
    }
}
