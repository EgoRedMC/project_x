﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class NPC
    {
        public int X = 1;
        public int Y = 1;
        public int usinglevel = 0;
        public string Name = "Чел";
        public NPCQuests Quests = new NPCQuests();
        //
        public NPC()
        { }
        public NPC(NPC npcstart, NPC[] npcend, string[] questname, string[] questtalk, string[] questtalkend,int[] exp)
        { Quests = new NPCQuests(npcstart, npcend, questname, questtalk, questtalkend, exp); }
        //
        public void NPCSetQuests(NPC npcstart, NPC[] npcend, string[] questname, string[] questtalk, string[] questtalkend, int[] exp)
        { Quests = new NPCQuests(npcstart, npcend, questname, questtalk, questtalkend, exp); }
    }
}
