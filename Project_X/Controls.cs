﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class Controls
    {
        public enum button
        { W, A, S, D, E, Esc, M, J, LEFTARROW, RIGHTARROW, UPARROW, DOWNARROW, D1, D2, D3, D4, D5, D6, D7, D8, D9, D0 }
        public button Button;
        public ConsoleKeyInfo key;
        public Levels.ife Ife = Levels.ife.NULL;
        private int npcX = 0, npcY = 0;
        //
        public Controls()
        { }
        //
        public int FindNPC(NPCOnLevel npc, int npcX, int npcY)
        {
            int npcnumber = -1;
            for (int i = 0; i < npc.npc.Length; i++)
            {
                if ((npc.npc[i].Y == npcY) & (npc.npc[i].X == npcX))
                    npcnumber = i;
            }
            return npcnumber;
        }
        public void WhatButton()
        {
            switch (key.Key)
            {
                case ConsoleKey.W:
                    Button = button.W;
                    break;
                case ConsoleKey.S:
                    Button = button.S;
                    break;
                case ConsoleKey.A:
                    Button = button.A;
                    break;
                case ConsoleKey.D:
                    Button = button.D;
                    break;
                case ConsoleKey.E:
                    Button = button.E;
                    break;
                case ConsoleKey.M:
                    Button = button.M;
                    break;
                case ConsoleKey.J:
                    Button = button.J;
                    break;
                case ConsoleKey.Escape:
                    Button = button.Esc;
                    break;
                case ConsoleKey.D1:
                    Button = button.D1;
                    break;
                case ConsoleKey.D2:
                    Button = button.D1;
                    break;
                case ConsoleKey.D3:
                    Button = button.D1;
                    break;
                case ConsoleKey.D4:
                    Button = button.D1;
                    break;
                case ConsoleKey.D5:
                    Button = button.D1;
                    break;
                case ConsoleKey.D6:
                    Button = button.D1;
                    break;
                case ConsoleKey.D7:
                    Button = button.D1;
                    break;
                case ConsoleKey.D8:
                    Button = button.D1;
                    break;
                case ConsoleKey.D9:
                    Button = button.D1;
                    break;
                case ConsoleKey.D0:
                    Button = button.D1;
                    break;
                case ConsoleKey.UpArrow:
                    Button = button.UPARROW;
                    break;
                case ConsoleKey.DownArrow:
                    Button = button.DOWNARROW;
                    break;
                case ConsoleKey.LeftArrow:
                    Button = button.LEFTARROW;
                    break;
                case ConsoleKey.RightArrow:
                    Button = button.RIGHTARROW;
                    break;
            }
        }
        public void ButtonDo(int usinglevel, NPCOnLevel npc, Player player, Levels levels)
        {
            Console.Write("                                               ");
            if ((Button == button.W) | (Button == button.S) | (Button == button.A) | (Button == button.D))
            {
                player.Point.SetDirection(Button);
                if (levels.Go(player, Button))
                {
                    player.Point.Move(Button);
                    player.Point.PlaceThis(levels);
                }
                if (levels.ifE(player, ref npcX, ref npcY) == Levels.ife.NPC)
                {
                    Console.SetCursorPosition(0,levels.fields[usinglevel].Data.Length);
                    Console.WriteLine(npc.npc[FindNPC(npc, npcX, npcY)].Name);
                }
            }
            if (Button == button.J)
                Player.MyQuests.ShowTakenQuests(usinglevel, npc, player, levels);
            if (Button == button.E)
            {
                if (levels.ifE(player, ref npcX, ref npcY) == Levels.ife.NPC)
                {
                    Console.SetCursorPosition(0, levels.fields[player.Point.PlaceNumber].Data.Length);
                    npc.npc[FindNPC(npc, npcX, npcY)].Quests.ShowWindow(usinglevel, npc, player, levels, npc.npc[FindNPC(npc, npcX, npcY)]);
                }
                if (levels.ifE(player, ref npcX, ref npcY) == Levels.ife.DOOR)
                    levels.doorOpen(player);
            }
            levels.doorClose(player);
            if (Button == button.Esc)
            { ExitMenu(usinglevel, npc, player, levels); }
        }
        private void ExitMenu(int usinglevel, NPCOnLevel npc, Player player, Levels levels)
        {
            bool b = true;
            ConsoleKeyInfo key;
            while (b)
            {
                Console.Clear();
                Console.WriteLine("1. Продолжить");
                Console.WriteLine("2. Выход");
                key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        Console.Clear();
                        levels.Print(usinglevel);
                        npc.Print(levels, player);
                        b = !b;
                        break;
                    case ConsoleKey.D2:
                        Environment.Exit(1);
                        break;
                }
            }
        }
    }
}
