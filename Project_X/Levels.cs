﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class Levels
    {
        private void drawField(int[][] field)
        {
            for (int i = 0; i < field.Length; i++)
            {
                for (int j = 0; j < field[0].Length; j++)
                    Console.Write(drawGField(field[i][j]));
                Console.WriteLine();
            }
        }
        private char drawGField(int number)
        {
            char temp = ' ';
            if (number == 1)
                temp = '█';
            if (number == 2)
                temp = '‼';
            if (number == 3)
                temp = '&';
            if (number == 4)
                temp = ' ';
            if (number == 5)
                temp = ' ';
            if (number == 6)
                temp = ' ';
            if (number == 7)
                temp = ' ';
            if (number == 8)
                temp = ' ';
            if (number == 9)
                temp = ' ';
            return temp;
        }
        //
        public Matrix[] fields = new Matrix[10];
        public int doorX = 0, doorY = 0, doorcount = 0;
        public bool doorbool = false;
        public enum ife
        { NPC, DOOR, NULL }
        //
        public Levels(int num)
        {
            fields = new Matrix[num];
            for (int i = 0; i < num; i++)
                fields[i] = new Matrix();
            fields[0].ReadFromFile("0.txt");
        }
        //
        public void Write(int num)
        {
            if (num < fields.Length)
                fields[num].Print();
        }
        public void Print(int num)
        {
            if (num < fields.Length)
            {
                drawField(fields[num].Data);
            }
        }
        public bool Go(Player player, Controls.button Button)
        {
            bool temp = false;
            switch (Button)
            {
                case Controls.button.W:
                    if (fields[player.Point.PlaceNumber].Data[player.Point.Y - 1][player.Point.X] == 0)
                        temp = true;
                    break;
                case Controls.button.S:
                    if (fields[player.Point.PlaceNumber].Data[player.Point.Y + 1][player.Point.X] == 0)
                        temp = true;
                    break;
                case Controls.button.A:
                    if (fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X - 1] == 0)
                        temp = true;
                    break;
                case Controls.button.D:
                    if (fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X + 1] == 0)
                        temp = true;
                    break;
            }
            return temp;
        }
        public ife ifE(Player player, ref int npcX, ref int npcY)
        {
            ife Temp = ife.NULL;
            switch (player.Point.Direction)
            {
                case Point.direction.RIGHT:
                    if ((fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X + 1] != 0) & (fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X + 1] != 1))
                        switch (fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X + 1])
                        {
                            case 2:
                                Temp = ife.DOOR;
                                break;
                            case 10:
                                npcX = player.Point.X + 1;
                                npcY = player.Point.Y;
                                Temp = ife.NPC;
                                break;
                        }
                    break;
                case Point.direction.LEFT:
                    if ((fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X - 1] != 0) & (fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X - 1] != 1))
                        switch (fields[player.Point.PlaceNumber].Data[player.Point.Y][player.Point.X - 1])
                        {
                            case 2:
                                Temp = ife.DOOR;
                                break;
                            case 10:
                                npcX = player.Point.X - 1;
                                npcY = player.Point.Y;
                                Temp = ife.NPC;
                                break;
                        }
                    break;
                case Point.direction.UP:
                    if ((fields[player.Point.PlaceNumber].Data[player.Point.Y - 1][player.Point.X] != 0) & (fields[player.Point.PlaceNumber].Data[player.Point.Y - 1][player.Point.X] != 1))
                        switch (fields[player.Point.PlaceNumber].Data[player.Point.Y - 1][player.Point.X])
                        {
                            case 2:
                                Temp = ife.DOOR;
                                break;
                            case 10:
                                npcX = player.Point.X;
                                npcY = player.Point.Y - 1;
                                Temp = ife.NPC;
                                break;
                        }
                    break;

                case Point.direction.DOWN:
                    if ((fields[player.Point.PlaceNumber].Data[player.Point.Y + 1][player.Point.X] != 0) & (fields[player.Point.PlaceNumber].Data[player.Point.Y + 1][player.Point.X] != 1))
                        switch (fields[player.Point.PlaceNumber].Data[player.Point.Y + 1][player.Point.X])
                        {
                            case 2:
                                Temp = ife.DOOR;
                                break;
                            case 10:
                                npcX = player.Point.X;
                                npcY = player.Point.Y + 1;
                                Temp = ife.NPC;
                                break;
                        }
                    break;
            }
            return Temp;
        }
        public void doorOpen(Player player)
        {
            switch (player.Point.Direction)
            {
                case Point.direction.DOWN:
                    doorX = player.Point.X;
                    doorY = player.Point.Y + 1;
                    fields[player.Point.PlaceNumber].Data[doorY][doorX] = 0;
                    Console.SetCursorPosition(doorX, doorY);
                    Console.Write(' ');
                    break;
                case Point.direction.UP:
                    doorX = player.Point.X;
                    doorY = player.Point.Y - 1;
                    fields[player.Point.PlaceNumber].Data[doorY][doorX] = 0;
                    Console.SetCursorPosition(doorX, doorY);
                    Console.Write(' ');
                    break;
                case Point.direction.LEFT:
                    doorX = player.Point.X - 1;
                    doorY = player.Point.Y;
                    fields[player.Point.PlaceNumber].Data[doorY][doorX] = 0;
                    Console.SetCursorPosition(doorX, doorY);
                    Console.Write(' ');
                    break;
                case Point.direction.RIGHT:
                    doorX = player.Point.X + 1;
                    doorY = player.Point.Y;
                    fields[player.Point.PlaceNumber].Data[doorY][doorX] = 0;
                    Console.SetCursorPosition(doorX, doorY);
                    Console.Write(' ');
                    break;
            }
            doorbool = true;
        }
        public void doorClose(Player player)
        {
            if (doorbool)
            {
                doorcount++;
                if (doorcount >= 3)
                {
                    fields[player.Point.PlaceNumber].Data[doorY][doorX] = 2;
                    Console.SetCursorPosition(doorX, doorY);
                    Console.Write('‼');
                    doorbool = false;
                    doorcount = 0;
                }
            }
            if (!doorbool)
                if ((doorX != 0) & (doorY != 0))
                {
                    Console.SetCursorPosition(doorX, doorY);
                    Console.Write('‼');
                }
        }
    }
}
