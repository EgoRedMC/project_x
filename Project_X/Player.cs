﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class Player
    {
        Random r = new Random();
        public Common Main = new Common();
        public Point Point = new Point();
        public static TakenQuests MyQuests = new TakenQuests();
        //
        public Player()
        { }
        public Player(int x, int y)
        {
            Point.X = x;
            Point.Y = y;
        }
        public Player(int x, int y, int strength, int agility, int lucky, int endurance)
        {
            Point.X = x;
            Point.Y = y;
            Main.Strength = strength;
            Main.Lucky = lucky;
            Main.Agility = agility;
            Main.Endurance = endurance;
            Main.HP = this.Main.SetHP();
        }
    }
}
