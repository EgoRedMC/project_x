﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class Mob
    {
        Random r = new Random();
        Common Main = new Common();
        Point Point = new Point();
        //
        public Mob()
        { }
        public Mob(int x, int y)
        {
            Point.X = x;
            Point.Y = y;
        }
        public Mob(int x, int y, int strength, int agility, int lucky, int endurance)
        {
            Point.X = x;
            Point.Y = y;
            Main.Strength = strength;
            Main.Lucky = lucky;
            Main.Agility = agility;
            Main.Endurance = endurance;
            Main.HP = this.Main.SetHP();
        }
        //
        
    }
}
