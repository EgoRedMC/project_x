﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class NPCQuests
    {
        public QuestGo[] Quests;
        string took = "\t\tВзято";
        string done = "\t\tВыполнено";
        //
        public NPCQuests()
        { }
        public NPCQuests(NPC npcstart, NPC[] npcend,string[] questname ,string[] questtalk,string[] questtalkend,int[] exp)
        {
            Quests = new QuestGo[npcend.Length];
            for (int i = 0; i < npcend.Length; i++)
            {
                Quests[i] = new QuestGo(npcstart, npcend[i],questname[i],questtalk[i],questtalkend[i],exp[i]);
            }
        }
        //
        public void ShowWindow(int usinglevel, NPCOnLevel npc, Player player, Levels levels, NPC thisnpc)
        {
            bool c = true;
            bool isTaken = false;
            int[] taken = new int[1000];
            for (int i = 0; i < taken.Length; i++)
            { taken[i] = -1; }
            int takennumber = 0;
            for (int i = 0; i < Player.MyQuests.count; i++)
            {
                if (Player.MyQuests.Taken[i] != null)
                    if (Player.MyQuests.Taken[i].npcEnd == thisnpc)
                    {
                        isTaken = true;
                        taken[takennumber] = i;
                        takennumber++;
                    }
            }
            if (Quests != null)
            {
                bool avalible = false;
                for (int i = 0; i < Quests.Length; i++)
                {
                    if (!Quests[i].Done)
                        avalible = true;
                }
                //
                if (avalible)
                    while (c)
                    {
                        Console.Clear();
                        Console.WriteLine(Quests[0].npcStart.Name);
                        for (int i = 0; i < Quests.Length; i++)
                        {
                            if ((!Quests[i].Took) & (!Quests[i].Done))
                                Console.WriteLine(" [ ] {0}", Quests[i].QuestName);
                            else
                            {
                                if (Quests[i].Done)
                                    Console.WriteLine(" [ ] {0}{1}", Quests[i].QuestName, done);
                                else
                                    Console.WriteLine(" [ ] {0}{1}", Quests[i].QuestName, took);
                            }
                        }
                        if (isTaken)
                        {
                            for (int i = 0; i < takennumber; i++)
                            { Console.WriteLine(" [ ] {0}", Player.MyQuests.Taken[taken[i]].QuestName); }
                        }
                        Console.WriteLine(" [ ] Выход");
                        //
                        ConsoleKeyInfo key;
                        bool b = true;
                        int num = 0;
                        Console.SetCursorPosition(2, num+1);
                        Console.Write(" ");
                        while (b)
                        {
                            Console.SetCursorPosition(2, num+1);
                            Console.Write("x");
                            key = Console.ReadKey(true);
                            Console.SetCursorPosition(2, num+1);
                            Console.Write(" ");
                            switch (key.Key)
                            {
                                case ConsoleKey.Escape:
                                    b = false;
                                    c = false;
                                    break;
                                case ConsoleKey.UpArrow:
                                    if (num > 0)
                                        num--;
                                    break;
                                case ConsoleKey.DownArrow:
                                    if (num < takennumber + Quests.Length)
                                        num++;
                                    break;
                                case ConsoleKey.Enter:
                                    if (num == takennumber + Quests.Length)
                                    {
                                        b = false;
                                        c = false;
                                    }
                                    for (int i = 0; i < Quests.Length; i++)
                                    {
                                        if (num == i)
                                            if (!Quests[i].Took)
                                            {
                                                Console.Clear();
                                                Console.WriteLine("{0}", Quests[i].QuestTalk);
                                                Console.WriteLine("1. Ладно, я это сделаю");
                                                Console.WriteLine("2. Нет уж, спасибо");
                                                while (b)
                                                {
                                                    ConsoleKeyInfo key1 = Console.ReadKey(true);
                                                    b = false;
                                                    switch (key1.Key)
                                                    {
                                                        case ConsoleKey.D1:
                                                            Quests[i].Took = true;
                                                            Player.MyQuests.PutInQuest(Quests[i]);
                                                            break;
                                                        case ConsoleKey.D2:
                                                            break;
                                                        default:
                                                            b = true;
                                                            break;
                                                    }
                                                }
                                            }
                                    }
                                    for (int i = 0; i < takennumber; i++)
                                    {
                                        if (num == i + Quests.Length)
                                        {
                                            Console.Clear();
                                            Console.WriteLine("{0}", Player.MyQuests.Taken[taken[i]].QuestTalkEnd);
                                            Player.MyQuests.EraseQuest(taken[i]);
                                            Console.ReadKey(true);
                                        }
                                    }
                                    break;
                            }
                        }
                    }
            }
            //
            if ((Quests == null) & (isTaken))
            {
                Console.Clear();
                while (c)
                {
                    isTaken = false;
                    for (int i = 0; i < Player.MyQuests.count; i++)
                    {
                        if (Player.MyQuests.Taken[i] != null)
                        {
                            if (Player.MyQuests.Taken[i].npcEnd == thisnpc)
                                isTaken = true;
                            c = false;
                        }
                    }
                    if (isTaken)
                    {
                        Console.WriteLine(Player.MyQuests.Taken[taken[0]].npcEnd.Name);
                        for (int i = 0; i < takennumber; i++)
                        {
                            Console.WriteLine(" [ ] {0}", Player.MyQuests.Taken[taken[i]].QuestName);
                        }
                        Console.WriteLine(" [ ] Выход");
                        ConsoleKeyInfo key;
                        bool b = true;
                        int num = 0;
                        Console.SetCursorPosition(2, num+1);
                        Console.Write(" ");
                        while (b)
                        {
                            Console.SetCursorPosition(2, num+1);
                            Console.Write("x");
                            key = Console.ReadKey(true);
                            Console.SetCursorPosition(2, num+1);
                            Console.Write(" ");
                            switch (key.Key)
                            {
                                case ConsoleKey.UpArrow:
                                    if (num > 0)
                                        num--;
                                    break;
                                case ConsoleKey.DownArrow:
                                    if (num < takennumber)
                                        num++;
                                    break;
                                case ConsoleKey.Enter:
                                    if (num == takennumber)
                                    {
                                        b = false;
                                        c = false;
                                    }
                                    for (int i = 0; i < takennumber; i++)
                                    {
                                        if (num == i)
                                        {
                                            Console.Clear();
                                            Console.WriteLine("{0}", Player.MyQuests.Taken[taken[i]].QuestTalkEnd);
                                            for (int j = 0; j < Player.MyQuests.Taken[taken[i]].npcStart.Quests.Quests.Length; j++)
                                            {
                                                if ((Player.MyQuests.Taken[taken[i]].npcStart.Quests.Quests[j].QuestName == Player.MyQuests.Taken[taken[i]].QuestName) & (Player.MyQuests.Taken[taken[i]].npcStart.Quests.Quests[j].npcEnd == Player.MyQuests.Taken[taken[i]].npcEnd))
                                                    Player.MyQuests.Taken[taken[i]].npcStart.Quests.Quests[j].Did();
                                            }
                                            Player.MyQuests.EraseQuest(taken[i]);
                                            Console.ReadKey(true);
                                            b = false;
                                        }
                                    }
                                    Console.Clear();
                                    break;
                            }
                        }
                    }
                }
            }

            Console.Clear();
            levels.Print(usinglevel);
            npc.Print(levels, player);
        }
    }
}
