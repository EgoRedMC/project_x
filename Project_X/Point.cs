﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class Point
    {

        public int X = 1;
        public int Y = 1;
        public char icon = '☺';
        public enum direction
        { UP, DOWN, RIGHT, LEFT }
        public direction Direction;
        public int PlaceNumber = 0;
        /*public enum place
        { DESERT, STARTCAVE, VILLAGE, CITY }*/
        //
        public Point()
        { }
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        //
        public void SetDirection(Controls.button Button)
        {
            switch (Button)
            {
                case Controls.button.W:
                    Direction = direction.UP;
                    break;
                case Controls.button.S:
                    Direction = direction.DOWN;
                    break;
                case Controls.button.A:
                    Direction = direction.LEFT;
                    break;
                case Controls.button.D:
                    Direction = direction.RIGHT;
                    break;
            }
        }
        public void Move(Controls.button Button)
        {
            switch (Button)
            {
                case Controls.button.W:
                    Console.SetCursorPosition(X, Y);
                    Console.Write(" ");
                    Y--;
                    break;
                case Controls.button.S:
                    Console.SetCursorPosition(X, Y);
                    Console.Write(" ");
                    Y++;
                    break;
                case Controls.button.A:
                    Console.SetCursorPosition(X, Y);
                    Console.Write(" ");
                    X--;
                    break;
                case Controls.button.D:
                    Console.SetCursorPosition(X, Y);
                    Console.Write(" ");
                    X++;
                    break;
            }
        }
        public void PlaceThis(Levels levels)
        {
            Console.SetCursorPosition(X, Y);
            Console.Write(icon);
            Console.SetCursorPosition(0, levels.fields[this.PlaceNumber].Data.Length);
        }
    }
}
