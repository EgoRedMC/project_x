﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_X
{
    class Common
    {
        Random r = new Random();
        public int HP;
        public enum animal
        { DOG, CAT }
        public animal Animal;
        public int Strength = 5;
        public int Agility = 5;
        public int Endurance = 5;
        public int Lucky = 5;
        public enum toDo
        { DODGE, BODYCHECK, HARDHIT }
        public toDo ToDo;
        //
        public Common()
        { }
        //
        public int SetHP()
        {
            return Endurance*20;
        }
        public int CountDamage()
        {
            int damage = Strength * 3;

            if (ToDo == toDo.HARDHIT)
            {
                if (r.Next(100) >= (12 - Strength) * 10)
                    damage = Strength * 5;
            }
            return damage;
        }
        public void GetDamage(int damage)
        {
            switch (ToDo)
            {
                case toDo.DODGE:
                    if (r.Next(100) <= Agility * 8)
                        damage = 0;
                    break;
                case toDo.BODYCHECK:
                    if (r.Next(100) <= Strength * 8)
                        damage -= (damage/3)*2;
                    break;
            }
        }
    }
}
