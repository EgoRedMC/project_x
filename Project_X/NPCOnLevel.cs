﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Project_X
{
    class NPCOnLevel
    {
        public NPC[] npc;
        public int npccount = 0;
        //
        public NPCOnLevel(string filename)
        {
            int linecount = 1;
            StreamReader stream1 = new StreamReader(filename);
            string line = null;
            while ((line = stream1.ReadLine()) != "404")
            {
                if (line == "//")
                    npccount++;
            }
            stream1.Close();
            npc = new NPC[npccount];
            for (int i = 0; i < npccount; i++)
                npc[i] = new NPC();
            npccount = 0;
            StreamReader stream2 = new StreamReader(filename);
            while ((line = stream2.ReadLine()) != "404")
            {
                switch (linecount)
                {
                    case 1:
                        npc[npccount].X = int.Parse(line);
                        break;
                    case 2:
                        npc[npccount].Y = int.Parse(line);
                        break;
                    case 3:
                        npc[npccount].Name = line;
                        break;
                }
                if (line == "//")
                {
                    linecount = 0;
                    npccount++;
                }
                linecount++;
            }
            stream2.Close();
        }
        //
        private NPC[] MakeNPCArrayBigger(NPC[] array)
        {
            NPC[] array1 = new NPC[array.Length + 1];
            for (int i = 0; i < array.Length; i++)
            {
                array1[i] = new NPC();
                array1[i + 1] = new NPC();
                array1[i] = array[i];
            }
            //
            return array1;
        }
        private string[] MakeStringArrayBigger(string[] array)
        {
            string[] array1 = new string[array.Length + 1];
            for (int i = 0; i < array.Length; i++)
            {
                array1[i] = "";
                array1[i + 1] = "";
                array1[i] = array[i];
            }
            //
            return array1;
        }
        private int[] MakeIntArrayBigger(int[] array)
        {
            int[] array1 = new int[array.Length + 1];
            for (int i = 0; i < array.Length; i++)
            {
                array1[i] = 0;
                array1[i + 1] = 0;
                array1[i] = array[i];
            }
            //
            return array1;
        }

        private NPC[] NormalNPCArray()
        {
            NPC[] array1 = new NPC[1];
            array1[0] = new NPC();
            //
            return array1;
        }
        public void SetQuests(string filename)
        {
            int linecount = 1;
            string line = null;
            NPC[] arrayNPC = new NPC[1];
            arrayNPC[0] = new NPC();
            string[] arrayname = new string[1];
            string[] arraytalk = new string[1];
            string[] arraytalkend = new string[1];
            int[] arrayexp = new int[1];
            int tempthislevel = 0;
            int tempnpclevel = 0;
            int tempnpcnumber = 0;
            int npclinecount = 0;
            int questcount = 0;
            bool anyquests = false;
            npccount = 0;
            StreamReader stream2 = new StreamReader(filename);
            while ((line = stream2.ReadLine()) != "404")
            {
                if (anyquests)
                {
                    if (linecount == 5)
                        tempthislevel = int.Parse(line);
                    switch (npclinecount + 1)
                    {
                        case 1:
                            tempnpclevel = int.Parse(line);
                            break;
                        case 2:
                            tempnpcnumber = int.Parse(line);
                            arrayNPC[questcount] = Program.npc[tempnpclevel].npc[tempnpcnumber];
                            break;
                        case 3:
                            arrayname[questcount] = line;
                            break;
                        case 4:
                            arraytalk[questcount] = line;
                            break;
                        case 5:
                            arraytalkend[questcount] = line;
                            break;
                        case 6:
                            arrayexp[questcount] = int.Parse(line);
                            break;
                    }
                    //
                    npclinecount++;
                }
                if (line == "/")
                {
                    anyquests = true;
                    npclinecount = 0;
                    questcount = 0;
                }
                if (line == "//")
                {
                    linecount = 0;
                    if (anyquests)
                        Program.npc[tempthislevel].npc[npccount].Quests = new NPCQuests(npc[npccount], arrayNPC, arrayname, arraytalk, arraytalkend, arrayexp);
                    anyquests = false;
                    npccount++;
                    arrayNPC = NormalNPCArray();
                    arrayname = new string[1];
                    arraytalk = new string[1];
                    arraytalkend = new string[1];
                    arrayexp = new int[1];
                }
                if (line == "/.")
                {
                    arrayNPC = MakeNPCArrayBigger(arrayNPC);
                    arrayname = MakeStringArrayBigger(arrayname);
                    arraytalk = MakeStringArrayBigger(arraytalk);
                    arraytalkend = MakeStringArrayBigger(arraytalkend);
                    arrayexp = MakeIntArrayBigger(arrayexp);
                    npclinecount = 0;
                    questcount++;
                }
                linecount++;
            }
            stream2.Close();
        }
        public void Print(Levels levels, Player player)
        {
            for (int i = 0; i < npc.Length; i++)
            {
                Console.SetCursorPosition(npc[i].X, npc[i].Y);
                Console.Write("&");
                levels.fields[player.Point.PlaceNumber].Data[npc[i].Y][npc[i].X] = 10;
            }
        }
    }
}